import "./App.css";

import { useState, useEffect } from 'react';

function App() {
	
	const [ count, setCount ] = useState(1);
	
	useEffect(() => {
			document.title = count;
		}, [count]);
	
  return (
    <div className="App">
      <section className="hero">
        <div className="hero-body">
          <p className="title">A React Task</p>
          <p className="subtitle">by Boom.dev</p>
          <button onClick={() => { setCount(prevCount => prevCount + 1)}}>Count({count})</button>
        </div>
      </section>
      <div className="container is-fullhd">
        <div className="notification">
          Edit the <code>./src</code> folder to add components.
        </div>
      </div>
    </div>
  );
}

export default App;
